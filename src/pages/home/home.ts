import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Restangular } from 'ngx-restangular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public restangular: Restangular) {

  }

  ngOnInit(){
    console.log('hey.');
    this.restangular.all('posts').getList()
    .subscribe( posts => {
      console.log(posts);
      posts.forEach(function(post){
        console.log(post);
      });
    });
  }

}
