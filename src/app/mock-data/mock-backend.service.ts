import { HttpHandler, HttpRequest, HttpEvent, HttpErrorResponse, HttpHeaders, HttpResponse, HttpBackend } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { RequestShowService } from "../request-show-service/request-show.service";
import { Injectable } from "@angular/core";

@Injectable()
export class MockBackendService extends HttpHandler {

  realHttp: HttpBackend = null;

  constructor(private requestShowService: RequestShowService) {
    super();
  }

  setRealHttp(realHttp: HttpBackend){
    this.realHttp = realHttp;
  }
  
  handle(req: HttpRequest<any>): Observable<HttpEvent<any>> {

    let dejarPasar = false;
    if (dejarPasar){
      return this.realHttp.handle(req);
    }

    let body: any = [{user: "first"}, {user: "second"}, {user: "third"}];
    let headers = new HttpHeaders({
      'header': 'server-header'
    });

    this.requestShowService.requestToShow.next(req);

    /*
      Ejemplos de cómo hacer los mocks:
      https://github.com/2muchcoffeecom/ngx-restangular/blob/e23c9b4d8e5a2cdfd3f11b5a2e5fefe65eef0509/demo/mock-data/mock-backend.service.ts
     */

    console.log("Request Url on Backend: ", req.urlWithParams);

    return Observable.of(new HttpResponse({ body, headers, status: 200})).delay(0);
  }
}
