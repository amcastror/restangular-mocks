import { HttpBackend } from "@angular/common/http";
// import { RestangularHttp } from "../../src/ngx-restangular-http"; // esto no debería funcionar. Debería salir del mismo lugar que Restangular.
import { RestangularHttp } from 'ngx-restangular';
import { MockBackendService } from "./mock-backend.service";

export const MockProviders = [
  MockBackendService,
  {
    provide: RestangularHttp,
    useFactory: (http: MockBackendService, realHttp: HttpBackend) => {
      http.setRealHttp(realHttp);
      return new RestangularHttp(http);
    },
    deps: [MockBackendService, HttpBackend]
  }
];
