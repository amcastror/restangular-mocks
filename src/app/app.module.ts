import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { RestangularModule } from 'ngx-restangular';
import { MockProviders } from "./mock-data/mock-providers";
import { RequestShowService } from "./request-show-service/request-show.service"
import {HttpClientModule, HttpResponse, HttpHeaders} from "@angular/common/http";

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// https://jsonplaceholder.typicode.com/

export function RestangularConfigFactory (RestangularProvider) {
  RestangularProvider.setBaseUrl('https://jsonplaceholder.typicode.com');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    RestangularModule.forRoot(RestangularConfigFactory),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RequestShowService,
    MockProviders
  ]
})
export class AppModule {}
